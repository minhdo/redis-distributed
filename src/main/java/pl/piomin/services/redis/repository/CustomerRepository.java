package pl.piomin.services.redis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.piomin.services.redis.model.Customer;
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    //Customer findByExternalId(String externalId);
   // List<Customer> findByAccountsId(Long id);

}
